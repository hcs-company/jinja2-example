#!/usr/bin/env python3

import sys
from jinja2 import Environment, FileSystemLoader
from textwrap import indent

vars = {
    'naam': 'Wander',
    'tellen': 10,
    'getallen': [1, 2, 513, 132],
}



def render_template(template: str):
    print(f'Template: {template}')
    print()
    environment = Environment(loader=FileSystemLoader('templates'), lstrip_blocks=True, trim_blocks=True)
    try:
        template = environment.get_template(template)
    except Exception as e:
        sys.stderr.write(f'Probleem met het laden van het template {template}\n')
        print(e)
        sys.exit(1)
    print(indent(template.render(**vars), '    '))
    print()


if __name__ == '__main__':
    for template in sys.argv[1:]:
        render_template(template)

